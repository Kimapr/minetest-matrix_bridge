# Minetest Matrix bridge

This is a server-side mod for Minetest, creating a bridge between a Matrix instant-messaging room and a Minetest server.

This mod does not use any additional lua library, only standard Minetest API.

**Demo server**: `survival.juneland.fr` port `30000`; [see Juneland's Matrix room](https://txmn.tk/element/#/room/#juneland:matrix.txmn.tk)

[Donate Libre Currency Ğ1](https://demo.cesium.app/#/app/wot/ENA89PPrBHS8wxhxgGMZxUXd53nRw1BaXBDvCVmZ6Tip/)

[Minetest Forum thread](https://forum.minetest.net/viewtopic.php?f=11&t=27759)

## Install

The mod needs an existing account on the Matrix server, and access to the room.

Copy the template config to `config.lua`:

```bash
cp config.template.lua config.lua
```

then edit `config.lua`.

Add `matrix_bridge` to `secure.http_mods` (e.g. in the file `/etc/minetest/minetest.conf`):

```
secure.http_mods = matrix_bridge
```

### Compatibility proxy

If using Minetest `5.3.0` (probably any version strictly before `5.4.0`), because of a broken API, you need to run the proxy:

```bash
python3 proxy.py -A <matrix-host> -P <matrix-port> # --help to print options
```

In `config.lua`, set `MATRIX_SERVER=http://127.0.0.1:18448` and `MATRIX_HACK_PROXY=true`.

## Contributing

See [Matrix API docs](https://www.matrix.org/docs/guides/client-server-api).

## License

A bit of code comes from [diggers-mt/matrix_chat](https://github.com/diggers-mt/matrix_chat): BSD-2-Clause, Copyright 2017 Jon Neverland (joenas)

GNU AGPL v3, CopyLeft 2022 Pascal Engélibert (tuxmain), scuti

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.  
You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
